### Kërkesa

PANGRAM quhet një tekst që i përdor të paktën një herë të gjitha
shkronjat e alfabetit anglisht.

Në një dokument të vjetër është shkruar diçka. Nëse teksti nuk është
PANGRAM ne mund të blejmë disa shkronja për ta plotësuar. Nqs dimë
edhe koston e çdo shkronje, sa është sasia më e vogël e parave që
duhet të shpenzojmë për ta kthyer tekstin në PANGRAM?

Referenca: https://www.codechef.com/problems/MATPAN

#### Shembull

```
$ cat input.txt
2
1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26
abcdefghijklmopqrstuvwz
1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26
thequickbrownfoxjumpsoverthelazydog

$ python3 prog.py < input.txt
63
0
```

Në rastin e parë mungojnë shkronjat **n** (çmimi 14), **x** (24) dhe
**y** (25).  Kështu që shuma që duhet të shpenzojmë është 14+24+25=63.

Në rastin e dytë nuk mungon asnjë nga shkronjat e alfabetit, kështu që
përgjigja është 0.

### Zgjidhja

```python
for _ in range(int(input())):
    prices = [int(i) for i in input().split()]
    text = input()
    letters = [0 for i in range(26)]
    for l in text:
        letters[ord(l) - ord('a')] += 1
    cost = 0
    for i in range(26):
        if letters[i] == 0:
            cost += prices[i]
    print(cost)
```

#### Sqarime

Funksioni `ord()` jep kodin e një shkronje në tabelën ASCII. Kurse
`ord(x) - ord('a')` i kthen shkronjat në një indeks që fillon nga 0.
Programi i kalon me radhë shkronjat e tekstit dhe numëron sa herë
përsëritet çdo shkronjë. Në fund kontrollon se cilat shkronja
përsëriten 0 herë dhe i shton çmimet e tyre në kosto.

### Detyra

Në një dyqan çokollatash shkon një turist. Shitësi dhe turisti nuk e
dinë gjuhën e njëri-tjetrit, por turisti tregon me dorë një lloj
çokollatash dhe i jep shitësit disa kartëmonedha. Duke ditur çmimin e
një çokollate shitësi mund ta marrë me mend se sa copë do që të blejë
turisti. Por nqs duke hequr një prej kartëmonedhave mund të blihet
përsëri i njëjti numër çokollatash, atere ky është një rast i paqartë
dhe shitësi nuk di çfarë të bëjë, kështu që ia kthen lekët mbrapsht
turistit.

Bëni një program që merr çmimin e një çokollate dhe kartëmonedhat që
jep turisti dhe gjen se sa copë çokollata do që të blejë turisti.
Nëse rasti është i paqartë programi duhet të nxjerrë -1.

Referenca: https://www.codechef.com/problems/BUYING2

#### Shembull

```
$ cat input.txt
3
4 7
10 4 8 5
1 10
12
2 10
20 50

$ python3 prog.py < input.txt
-1
1
7
```

Në rastin e parë, sasia gjithsej e parave është 27, dhe meqë çmimi
është 7, turisti mund të marrë vetëm 3 copë. Por nqs nuk do kishte dhënë
kartëmonedhën 5 përsëri do merrte 3 copë. Kështu që është rast i paqartë.

Në rastin e dytë sasia e parave është 12, çmimi 10, kështu që mund të
marrë 1 çokollatë.

Në rastin e tretë sasia e parave është 20+50, çmimi 10, mund të marrë
7 çokollata.

