### Kërkesa

Një listë **L** ka **N** numra të plotë **të ndryshëm nga zero**. Një
nën-listë quhet **alternuese** nëse çdo dy elemente fqinje të listës
kanë shenja të kundërta (njëra pozitive dhe tjetra negative).

Gjeni gjatësinë e nën-listës alternuese më të madhe të listës **L**.

#### Shembull

```
$ cat input.txt
3
4
1 2 3 4
4
1 -5 1 -5
6
-5 -1 -1 2 -2 -3

$ python3 prog.py < input.txt
1
4
3
```

### Zgjidhja

```python
T = int(input())
for t in range(T):
    N = int(input())
    L = list(map(int, input().split()))
    lmax = 0
    l = 0
    L.insert(0, L[0])
    L.append(L[-1])
    i = N+1
    while i > 0:
        if L[i-1] * L[i] < 0:
            l += 1
        else:
            if l > lmax:
                lmax = l
            l = 1
        i -= 1
    print(lmax)
```

### Detyra

Loli, vëllai i vogël i Colit, sapo ka mësuar disa prej shkronjave të
alfabetit, dhe mund të lexojë vetëm ato fjalë që përbëhen prej
shkronjave që njeh. Coli i jep atij një libër për të lexuar dhe do të
dijë se cilat prej fjalëve mund të lexojë Loli.

Referenca: https://www.codechef.com/problems/ALPHABET

#### Shembull

```
$ cat input.txt
act
2
cat
dog

$ python3 prog.py < input.txt
Yes
No
```

Rreshti i parë përmban shkronjat që ka mësuar Loli. Fjala e parë i ka
të gjitha shkronjat të njohura, fjala e dytë jo.
