#!/bin/bash
### fix images

cd $(dirname $0)
cd ..
mkdir -p images/

grep -e '!\[\](http' -R . | \
    while read line; do
        file=$(echo $line | cut -d: -f1)
        img="images/$(basename ${file%.md}).png"
        url=$(echo $line | cut -d'(' -f2 | cut -d')' -f1)
        echo $file - $img - $url
        wget -O $img $url
        sed -i $file -e "s#$url#$img#"
    done
