### Kërkesa

Cufi sapo ka marrë një lidhje interneti të re. Të dhënat e përdorimit
të internetit prej tij janë si më poshtë.

Në $`T_1`$ minutat e para shpejtësia e shkarkimit që ka përdorur është
$`D_1`$ MB në minutë, në $`T_2`$ minutat e tjera është $`D_2`$ MB/min,
e kështu me radhë deri në $`T_N`$ minutat e fundit kur shpejtësia ka
qenë $`D_N`$ MB/min.

Kompania e internetit i faturon 1 dollar për çdo 1 MB të shkarkuar, me
përjashtim të $`K`$ minutave të para, të cilat janë falas (si pjesë e
ofertës).

Gjeni sasinë totale të dollarëve që duhet të paguajë Cufi për
internetin e përdorur.

Referenca: https://www.codechef.com/problems/DWNLD

#### Shembull

```
$ cat input.txt
3
2 2
2 1
2 3
2 2
1 2
2 3
3 0
1 2
2 4
10 10

$ python3 prog.py < input.txt
6
3
110
```

Çdo rast testimi ka si rresht të parë numrat **N** dhe **K**, pastaj
vijnë **N** rreshta me të dhënat e shkarkimit **T** dhe **D**.

Në testin e parë, 2 minutat e para janë falas, kështu që kostoja
totale është: `2*3 = 6`.

Në testin e dytë, 2 minutat e para janë falas, kështu që kostoja
totale është: `1*3 = 3`.

Në rastin e tretë, 0 minuta janë falas, kështu që kostoja totale
është: `1*2 + 2*4 + 10*10 = 110`.

### Zgjidhja

```python
for _ in range(int(input())):
    n, k = map(int, input().split())
    total = 0
    for i in range(n):
        t, d = map(int, input().split())
        if k > 0:
            m = min(t, k)
            t -= m
            k -= m
        total += (t * d)
    print(total)
```

### Detyra

Cufit i pëlqen të luajë pingpong. Ai gjeti disa shënime me pikët e
lojrave që ka bërë para ca kohësh. Me **1** shënohet nëse ka fituar
një pikë dhe me **0** nëse ka humbur një pikë (e ka fituar
kundërshtari). Loja e pingpongut fitohet nga lojtari që bën i pari 11
pikë, me përjashtim të rastit kur të dy lojtarët kanë nga 10 pikë, në
të cilin loja vazhdon deri sa njëri prej lojtarëve të dalë 2 pikë para
kundërshtarit. Gjeni se cilat prej lojrave i ka fituar Cufi dhe cilat
i ka humbur.

Referenca: https://www.codechef.com/problems/TTENIS

#### Shembull

```
$ cat input.txt
2
0101111111111
11100000000000

$ python3 prog.py < input.txt
WIN
LOSE
```
