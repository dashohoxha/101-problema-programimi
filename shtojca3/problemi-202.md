### Kërkesa

https://codingcompetitions.withgoogle.com/codejam/round/00000000000000cb/0000000000007a30


### Zgjidhja

```python
import sys

M = 4
N = 5
board = {}

def go(x, y):
    if x == 0:    x += 1
    if x == M-1:  x -=1
    if y == 0:    y += 1
    if y == N-1:  y -= 1

    print(x+2, y+2)
    sys.stdout.flush()
    i, j = map(int, input().split())

    if j == -1 and j == -1:
        sys.exit()

    if i == 0 and j == 0:
        return True
    else:
        board[i-2, j-2] = True
        return False

def solve():
    # initialize the board
    for m in range(M):
        for n in range(N):
            board[m, n] = False

    # fill each cell of the board
    for m in range(M):
        for n in range(N):
            while not board[m, n]:
                if go(m, n): return

T = int(input())
for t in range(T):
    A = int(input())
    if A == 10:
        M = 3
        N = 4
    if A == 20:
        M = 4
        N = 5
    if A == 200:
        M = 14
        N = 15
    board = {}
    solve()
```

