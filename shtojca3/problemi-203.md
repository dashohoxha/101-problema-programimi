### Kërkesa

https://codingcompetitions.withgoogle.com/codejam/round/00000000000516b9/0000000000134c90

### Zgjidhja

```python
for t in range(int(input())):
    n = int(input())
    oponents = []
    for i in range(n):
        oponents.append(input())
    program = []
    pc = [0 for i in range(n)]
    while True:
        oponent_moves = set([oponents[i][pc[i]] for i in range(n) if oponents[i][pc[i]] != 'X'])
        if len(oponent_moves) == 3:
            print('Case #{}: IMPOSSIBLE'.format(t+1))
            break
        elif len(oponent_moves) == 1:
            if 'P' in oponent_moves:
                program.append('S')
            elif 'R' in oponent_moves:
                program.append('P')
            elif 'S' in oponent_moves:
                program.append('R')
            print('Case #{}: {}'.format(t+1, ''.join(program)))
            break
        elif len(oponent_moves) == 2:
            if 'R' in oponent_moves and 'P' in oponent_moves:
                program.append('P')
                mx = 'R'
            elif 'R' in oponent_moves and 'S' in oponent_moves:
                program.append('R')
                mx = 'S'
            elif 'P' in oponent_moves and 'S' in oponent_moves:
                program.append('S')
                mx = 'P'
            for i in range(n):
                if oponents[i][pc[i]] == mx:
                    oponents[i] = ['X']
        # advance the program counter to the next statement
        for i in range(n):
            pc[i] += 1
            pc[i] %= len(oponents[i])
```

