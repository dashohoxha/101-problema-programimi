### Kërkesa

Majkua është pjesëtar i shoqatës së koleksionistëve të pullave
postare.  Shoqata ka **N** pjesëtarë dhe secili prej tyre ka
**$`C_i`$** pulla në koleksionin e tij. Majkua pretendon se të gjitha
këto pulla poste mund të rishpërndahen në mënyrë të tillë që pjesëtari
**i** i shoqatës të ketë **i** pulla poste. Kontrolloni nëse Majkua ka
të drejtë.

Referenca: https://www.codechef.com/problems/DIVIDING

#### Shembull

```
$ cat input.txt
2
5
7 4 1 1 2
5
1 1 1 1 1

$ python3 prog.py < input.txt
YES
NO
```

### Zgjidhja 1

```python
for _ in range(int(input())):
    n = int(input())
    C = [int(c) for c in input().split()]
    s1 = s2 = 0
    for i in range(n):
        s1 += C[i]
        s2 += (i + 1)
    print('YES') if s1==s2 else print('NO')
```

### Zgjidhja 2

```python
for _ in range(int(input())):
    n = int(input())
    C = [int(c) for c in input().split()]
    print('YES') if sum(C) == n*(n + 1) // 2 else print('NO')
```
