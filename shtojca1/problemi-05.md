### Kërkesa

Bëni një program që merr gjatësinë dhe lartësinë e një drejtkëndëshi,
dhe gjen se kush është më i madh: sipërfaqja apo perimetri. Programi
duhet të nxjerrë 'Area' nëse është më e madhe sipërfaqja, 'Peri' nëse
është më i madh perimetri, ose 'Eq' nëse të dyja janë të
barabarta. Gjithashtu duhet të nxjerrë edhe vlerën më të madhe të
llogaritur (sipërfaqen ose perimetrin.

Referenca: https://www.codechef.com/problems/AREAPERI

#### Shembull

```
$ cat input.txt
3
1 2
5 6
4 4

$ python3 prog.py < input.txt

```
Peri 6
Area 30
Eq 16

### Zgjidhja

```python
t = int(input())
while t > 0:
    a, b = map(int, input().split())
    s = a * b
    p = a + b + a + b
    if s > p:
        print('Area', s)
    elif p > s:
        print('Peri', p)
    else:
        print('Eq', s)
    t -= 1
```
