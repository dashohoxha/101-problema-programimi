### Kërkesa

Bëni një program që gjen nëse 4 numra natyrorë **a**, **b**, **c**,
**d**, mund të jenë brinjë të një drejtkëndëshi.

Referenca: https://www.codechef.com/problems/RECTANGL

#### Shembull

```
$ cat3 input.txt
3
1 1 2 2
3 2 2 3
1 2 2 2

$ python3 prog.py < input.txt
YES
YES
NO
```

### Zgjidhja

```python
T = int(input())
for t in range(T):
    a, b, c, d = map(int, input().split())
    if (a == b and c == d) or (a == c and b == d) or (a == d and b == c):
        print('YES')
    else:
        print('NO')
```

