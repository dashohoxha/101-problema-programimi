### Kërkesa

Para se të fillojë mësimi mësuesi bën apelin, duke i thirrur emrat e
nxënësve një nga një. Nqs dy nxënës kanë të njëjtin emër (por mbiemra
të ndryshëm) atere mësuesi e thërret emrin të plotë, me emër dhe
mbiemër. Përndryshe thërret vetëm emrin (pa mbiemër).

Bëni një program që merr emrat e nxënësve dhe nxjerr se si i thërret
mësuesi ata.

Referenca: https://www.codechef.com/LTIME71B/problems/ATTND

#### Shembull

```
$ cat input.txt
1
4
hasan jaddouh
farhod khakimiyon
kerim kochekov
hasan khateeb

$ python3 prog.py < input.txt
hasan jaddouh
farhod
kerim
hasan khateeb
```

Kemi 1 rast testimi, i cili ka 4 emra.

### Zgjidhja 1

```python
for _ in range(int(input())):
    n = int(input())
    L = []
    F = []
    for i in range(n):
        first, last = input().split()
        L.append([first, last])
        F.append(first)
    for name in L:
        first, last = name
        if F.count(first) > 1:
            print(first, last)
        else:
            print(first)
```

Koha e rendit $`O(N^2)`$

### Zgjidhja 2

```python
for _ in range(int(input())):
    n = int(input())
    L = []
    F = {}
    for i in range(n):
        first, last = input().split()
        L.append([first, last])
        F[first] = F.get(first, 0) + 1
    for name in L:
        first, last = name
        if F[first] > 1:
            print(first, last)
        else:
            print(first)
```

Koha e rendit $`O(N*log(N))`$
