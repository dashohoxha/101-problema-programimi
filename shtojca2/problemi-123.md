### Kërkesa

Dejvi ka disa shokë të çuditshëm. Me rastin e ditëlindjes së tij
secili prej tyre i ka kërkuar që ta qerasë në një datë të caktuar, me
kushtin që do ta prishë shoqërinë në rast se nuk e qeras pikërisht në
atë datë. Mirëpo Dejvi nuk mund të qerasë më shumë se një prej tyre në
një datë të caktuar. Gjeni se me sa prej tyre mund ta ruajë shoqërinë.

Referenca: https://www.codechef.com/problems/CFRTEST

#### Shembull

```
$ cat input.txt
2
2
3 2
2
1 1

$ python3 prog.py < input.txt
2
1
```

Në rastin e parë, të dy shokët e kërkojnë qerasjen në data të
ndryshme, kështu që mund ta ruajë shoqërinë me të dy. Në rastin e
dytë, të dy e kërkojnë qerasjen në të njëjtën ditë, kështu që mund ta
ruajë shoqërinë vetëm me njërin prej tyre.

### Zgjidhja

```python
for _ in range(int(input())):
    input()
    print(len(set(map(int, input().split()))))
```

#### Sqarime

Nqs krijojmë një bashkësi me të dhënat (`set()`), atere secila prej
tyre do ruhet vetëm një herë, kështu që mjafton të gjejmë numrin e
tyre (me `len()`).