### Kërkesa

Cufit i pëlqen të luajë pingpong. Ai gjeti disa shënime me pikët e
lojrave që ka bërë para ca kohësh. Me **1** shënohet nëse ka fituar
një pikë dhe me **0** nëse ka humbur një pikë (e ka fituar
kundërshtari). Loja e pingpongut fitohet nga lojtari që bën i pari 11
pikë, me përjashtim të rastit kur të dy lojtarët kanë nga 10 pikë, në
të cilin loja vazhdon deri sa njëri prej lojtarëve të dalë 2 pikë para
kundërshtarit. Gjeni se cilat prej lojrave i ka fituar Cufi dhe cilat
i ka humbur.

Referenca: https://www.codechef.com/problems/TTENIS

#### Shembull

```
$ cat input.txt
2
0101111111111
11100000000000

$ python3 prog.py < input.txt
WIN
LOSE
```

### Zgjidhja 1

```python
for _ in range(int(input())):
    piket = input()
    p0 = p1 = 0
    for p in piket:
        if p == '0':
            p0 += 1
        else:
            p1 += 1
    if p1 > p0:
        print('WIN')
    else:
        print('LOSE')
```

### Zgjidhja 2

```python
for _ in range(int(input())):
    piket = input()
    p0 = piket.count('0')
    p1 = piket.count('1')
    print('WIN') if p1 > p0 else print('LOSE')
```

