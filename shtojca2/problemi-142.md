### Kërkesa

Labirinti më i thjeshtë në botë përbëhet nga një tabelë me përmasa
**NxN**. Duke filluar në cepin e majtë sipër, duhet vajtur në cepin e
djathtë poshtë, duke bërë vetëm lëvizje majtas (**E**ast) ose poshtë
(**S**outh).

Na jepen përmasat e tabelës dhe zgjidhja që ka bërë Lida. A mund të
gjeni një zgjidhje tjetër që është komplet e ndryshme nga ajo që ka
bërë Lida (dmth nqs zgjidhja e Lidës përmban një kalim nga qeliza A në
qelizën B, zgjidhja juaj nuk duhet të përmbajë një kalim të tillë).

Referenca: https://codingcompetitions.withgoogle.com/codejam/round/0000000000051705/00000000000881da

#### Shembull

```
$ cat input.txt
2
2
SE
5
EESSSESE

$ python3 prog.py < input.txt
Case #1: ES
Case #2: SEEESSES
```

Rasti i dytë është si në figurë:

![](images/eido2Aho.png)

### Zgjidhja

```python
for t in range(int(input())):
    input()
    p1 = input()
    p2 = []
    for c in p1:
        p2.append('E' if c=='S' else 'S')
    print('Case #{}: {}'.format(t+1, ''.join(p2)))
```

#### Sqarime

Mjafton të marrim simetriken e zgjidhjes së dhënë. Kjo prapë është
zgjidhje dhe është e garantuar që është komplet e ndryshme nga
zgjidhja e dhënë.
