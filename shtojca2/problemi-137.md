### Kërkesa

Një bankomat (ATM) ka brenda **K** para. Në radhë janë **N** klientë,
ku secili do që të tërheqë $`A_i`$ para. Nëse bankomati ka gjendje aq
sa kërkon klienti ose më shumë, ai ia jep lekët klientit, përndryshe
nxjerr një mesazh gabimi dhe klienti largohet pa marrë lekë. Gjeni për
secilin klient nëse i ka marrë lekët ose jo.

Referenca: https://www.codechef.com/problems/ATM2

#### Shembull

```
$ cat input.txt
2
5 10
3 5 3 2 1
4 6
10 8 6 4

$ python3 prog.py < input.txt
11010
0010
```

Kur klienti i merr lekët shënohet me **1**, përndryshe shënohet me
**0**.

Në rastin e parë janë 5 klientë dhe bankomati ka 10 lekë. Pasi merr
lekët klienti i parë dhe i dytë, në bankomat mbeten 2 lekë, të cilat
janë të pamjaftueshme për ti dhënë lekët klientit të tretë, por
mjaftojnë për ti dhënë lekët klientit të katërt.

Në rastin e dytë janë 4 klientë dhe bankomati ka 6 lekë. Dy klientët e
parë nuk mund ti tërheqin lekët, por i treti po. Pastaj nuk ngelet më
asnjë lekë, kështu që as klienti i katërt nuk mund ti tërheqë lekët.

### Zgjidhja

```python
for _ in range(int(input())):
    n, k = map(int, input().split())
    L = [int(i) for i in input().split()]
    for l in L:
        if l <= k:
            print(1, end='')
            k -= l
        else:
            print(0, end='')
    print()
```

