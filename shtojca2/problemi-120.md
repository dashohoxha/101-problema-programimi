### Kërkesa

Roboti Bani ka humbur rrugën. Ai ndodhet në kordinatat $`(x_1, y_1)`$
në një plan 2 dimensional, kurse shtëpia e tij ndodhet në kordinatat
$`(x_2, y_2)`$. Bëni një program që ta ndihmojë duke i treguar
drejtimin në të cilin duhet të lëvizë për të arritur në shtëpi. Nëse i
tregojmë një drejtim, roboti do të vazhdojë të lëvizë në atë drejtim
deri sa të arrijë në shtëpi. Janë katër drejtime që mund t'ia japim si
komandë: **majtas**, **djathtas**, **lartë**, **poshtë**. Nëse asnjë
nga këto drejtime nuk e çon robotin në shtëpi, programi duhet të
nxjerrë: **më vjen keq**.

Referenca: https://www.codechef.com/problems/ICPC16A

#### Shembull

```
$ cat input.txt
5
0 0 1 0
0 0 0 1
0 0 1 1
2 0 0 0
0 2 0 0

$ python3 prog.py < input.txt
right
up
sad
left
down
```

### Zgjidhja

```python
for _ in range(int(input())):
    x1, y1, x2, y2 = map(int, input().split())
    if x1 == x2:
        if y1 < y2:
            print('up')
        else:
            print('down')
    elif y1 == y2:
        if x1 < x2:
            print('right')
        else:
            print('left')
    else:
        print('sad')
```

